const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const axios = require('axios');
const colors = require('colors');

const { absolute_full_path, writeJSONSync, writeFileSync } = require('../utils');


module.exports = function(vorpal, apiOrigin) {

  function parseAxiosError(error) {
    if (error.response) {
      const {config, status, data} = error.response;
      return new Error(`axios error ${status} ${config.url}\n${JSON.stringify(data)}`);
    } else {
      return error;
    }
  }

  function writePageConfigs(pageconfigs, presetName) {
    for (var pageconfig of pageconfigs) {
      var page = pageconfig.page;
      var fullname = page.page_type == 'static' ? `${page.page_type}.${page.name}` : page.page_type;
      console.log(`pageconfig ${fullname} read`);
      var pageconfigDataFile = absolute_full_path(`presets/${presetName}/${fullname}.json`);
      var pageconfigData = {
        layout: pageconfig.layout,
        template: pageconfig.template,
        settings_data: pageconfig.settings_data,
      };
      writeJSONSync(pageconfigDataFile, pageconfigData);
    }
  }

  function writeTheme(theme, presetName) {
    var themeDataFile = absolute_full_path(`presets/${presetName}/theme.json`);
    var themeData = {
      assets: theme.assets,
      settings_data: theme.settings_data,
      private_settings_data: theme.private_settings_data
    };
    fs.readFile(themeDataFile, 'utf8', (err, content) => {
      if (err) {
        content = '{}';
        // throw err;
      }
      var newContent = Object.assign({
        name: presetName,
        title: presetName,
        description: presetName,
        color: '', demo: '', screenshot: '', screenshot_mobile: '', tags: []
      }, JSON.parse(content), themeData);
      writeJSONSync(themeDataFile, newContent);
    })
  }

  function writeThemeFiles(themefiles) {
    for (var themefile of themefiles) {
      var content = themefile.content;
      var file_type = themefile.file_type;
      var file_name = themefile.name;
      console.log(`${file_type}/${file_name}.njk read`);
      var themefileDataFile = absolute_full_path(`${file_type}/${file_name}.njk`);
      fs.writeFileSync(themefileDataFile, content + '\n', 'utf8');
      if (file_type === 'blocks') {
        console.log(`blocks/${file_name}.json read`);
        var blockConfigData = {
          name: `blocks/${themefile.name}`,
          title: themefile.title,
          icon: themefile.icon,
          metafield: themefile.metafield,
          settings_schema: themefile.settings_schema,
          settings_data: themefile.settings_data
        }
        if (themefile.permanent) {
          blockConfigData.permanent = true
        }
        if (themefile.presets.length) {
          blockConfigData.presets = themefile.presets
        }
        var blockConfigDataFile = absolute_full_path(`blocks/${file_name}.json`);
        writeJSONSync(blockConfigDataFile, blockConfigData);
      }
    }
  }

  function downloadPreset(shopName, themeId, presetName) {
    const ThemeAPI = axios.create({
      baseURL: `${apiOrigin}/api/shopthemes/theme/${themeId}/`,
      headers: { 'X-Shop': shopName }
    });
    return new Promise(function(resolve, reject) {
      axios.all([
        ThemeAPI.get(`/`),
        ThemeAPI.get(`/pageconfig/`)
      ]).then(axios.spread((resTheme, resPageconfigs) => {
        console.log(`Theme config read`);
        console.log(`${resPageconfigs.data.length} pageconfigs read`);
        writeTheme(resTheme.data, presetName);
        writePageConfigs(resPageconfigs.data, presetName);
        resolve();
      })).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  }

  function downloadThemeFiles(shopName, themeId, presetName) {
    const ThemeAPI = axios.create({
      baseURL: `${apiOrigin}/api/shopthemes/theme/${themeId}/`,
      headers: { 'X-Shop': shopName }
    });
    return new Promise(function(resolve, reject) {
      axios.all([
        ThemeAPI.get(`/`),
        ThemeAPI.get(`/file/`)
      ]).then(axios.spread((resTheme, resThemeFiles) => {
        console.log(`Theme config read`);
        console.log(`${resThemeFiles.data.length} themefiles read`);
        writeThemeFiles(resThemeFiles.data);
        resolve();
      })).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  }

  function listThemes(shopName) {
    const ThemeAPI = axios.create({
      baseURL: `${apiOrigin}/api/shopthemes/theme/`,
      headers: { 'X-Shop': shopName }
    });
    return new Promise((resolve, reject) => {
      ThemeAPI.get('/').then(function(response) {
        const themes = response.data.map((theme) => _.pick(theme, ['id', 'scope', 'name', 'title', 'private']));
        resolve(themes);
      }).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  }

  return {
    listThemes: listThemes,
    downloadPreset: downloadPreset,
    downloadThemeFiles: downloadThemeFiles
  }
}
