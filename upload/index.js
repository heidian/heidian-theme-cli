const path = require('path');
const axios = require('axios');
const _ = require('lodash');

const { absolute_full_path, readJSONSync } = require('../utils');
const Uploader = require('./uploader');


module.exports = function(vorpal, apiOrigin) {
  const themeConfig = readJSONSync(absolute_full_path('theme.json'));

  function uploadTheme(shopName, themeId, accessToken) {
    return new Promise(function(resolve, reject) {
      const uploader = Uploader(vorpal, {
        api_origin: apiOrigin,
        theme_id: themeId,
        auth: {
          shop_name: shopName,
          shop_access_token: accessToken
        }
      });
      uploader.upload()
        .then(() => resolve())
        .catch((err) => reject(err));
    });
  }

  function publishTheme() {
    return new Promise(function(resolve, reject) {
      const uploader = Uploader(vorpal, {
        api_origin: apiOrigin,
        theme_id: themeConfig.id,
        auth: {}
      });
      uploader.upload()
        .then(() => resolve())
        .catch((err) => reject(err));
    });
  }

  return {
    uploadTheme: uploadTheme,
    publishTheme: publishTheme,
  }
}
