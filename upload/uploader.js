const fs = require('fs');
const path = require('path');
const qiniu = require("qiniu");
const axios = require('axios');
const _ = require('lodash');

const { absolute_full_path, readJSONSync, listJSONFiles } = require('../utils');


module.exports = function(vorpal, options) {

  function parseAxiosError(error) {
    if (error.response) {
      const {config, status, data} = error.response;
      return new Error(`axios error ${status} ${config.url}\n${JSON.stringify(data)}`);
    } else {
      return error;
    }
  }

  const themeConfig = readJSONSync(absolute_full_path('theme.json'));

  const private_theme = !!options.auth.shop_name;

  const prefix = private_theme ? 'shopthemes' : 'themes';
  const headers = private_theme ? {
    'X-Shop': options.auth.shop_name,
    'Authorization': `Bearer ${options.auth.shop_access_token}`
  } : {};
  const api = axios.create({
    baseURL: `${options.api_origin}/api/`
  });
  const apiUpload = axios.create({
    baseURL: `${options.api_origin}/api/${prefix}/uploadtheme/${options.theme_id}/`,
    headers: headers
  });
  const apiTheme = axios.create({
    baseURL: `${options.api_origin}/api/${prefix}/theme/${options.theme_id}/`,
    headers: headers
  });

  var qiniuUptoken = '';
  const qiniuBucket = 'heidiantheme';
  const cdnOrigin = 'https://theme.heidiancdn.com';
  const cdnPath = `${Date.now()}/${themeConfig.name}/theme/assets`;
  const assets_prefix = `${cdnOrigin}/${cdnPath}/`;

  const upload_theme = function() {
    return new Promise(function(resolve, reject) {
      const steps = function() {
        uploadLayouts(themeConfig).then(() => {
          uploadTemplates(themeConfig).then(() => {
            uploadSnippets(themeConfig).then(() => {
              uploadBlocks(themeConfig).then(() => {
                uploadPresets(themeConfig).then(() => {
                  uploadLocales(themeConfig).then(() => {
                    uploadAssets(themeConfig).then(() => {
                      updateTheme(themeConfig).then(() => {
                        resolve();
                      }).catch((error) => reject(error))
                    }).catch((error) => reject(error))
                  }).catch((error) => reject(error))
                }).catch((error) => reject(error))
              }).catch((error) => reject(error))
            }).catch((error) => reject(error))
          }).catch((error) => reject(error))
        }).catch((error) => reject(error))
      };
      Promise.all([
        apiUpload.post('/reset/', { assets_prefix: assets_prefix }),
        api.get(`/clients/qiniu-uptoken/${qiniuBucket}/`)
      ]).then(([resReset, resQiniu]) => {
        qiniuUptoken = resQiniu.data.uptoken;
        // Chaining
        steps();
        // End chaining
      }).catch((error) => {
        reject(parseAxiosError(error));
      });
    })
  }

  const updateTheme = function(themeConfig) {
    return new Promise(function(resolve, reject) {
      apiUpload.patch('/', {
        settings_schema: themeConfig.settings_schema
      }).then(() => {
        vorpal.log('\u2713 (HeyShop) Theme updated');
        if (private_theme) {
          apiTheme.post('/apply_preset/', {
            preset: themeConfig.current_preset
          }).then(() => {
            vorpal.log(`\u2713 (HeyShop) Preset ${themeConfig.current_preset} applied`);
            resolve();
          });
        } else {
          resolve();
        }
      }).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  };

  const postThemeData = function(data, scope='file') {
    return new Promise(function(resolve, reject) {
      apiUpload.post(`/${scope}/`, data).then(() => {
        if (scope == 'file') {
          var extra = `${data.file_type}/${data.name}`;
        } else if (scope == 'preset') {
          var extra = data.name;
        } else if (scope == 'locale') {
          var extra = data.language;
        }
        vorpal.log(`\u2713 (HeyShop) ${scope} ${extra} posted to server`);
        resolve();
      }).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  };

  const uploadLayouts = function(themeConfig) {
    const tasks = [];
    (themeConfig.layouts || []).forEach((layout) => {
      var file = absolute_full_path(`layouts/${layout}.njk`);
      // vorpal.log(`read layout ${layout}\n    ${file}`);
      const promise = new Promise(function(resolve, reject) {
        fs.readFile(file, 'utf8', (err, content) => {
          if (err) {
            reject(err);
            return;
          }
          postThemeData({
            file_type: 'layouts',
            name: layout,
            title: layout,
            content: content
          }).then(resolve, reject);
        });
      });
      tasks.push(promise);
    });
    return Promise.all(tasks);
  };

  const uploadSnippets = function(themeConfig) {
    const tasks = [];
    (themeConfig.snippets || []).forEach((snippet) => {
      var file = absolute_full_path(`snippets/${snippet}.njk`);
      // vorpal.log(`read snippet ${snippet}\n    ${file}`);
      const promise = new Promise(function(resolve, reject) {
        fs.readFile(file, 'utf8', (err, content) => {
          if (err) {
            reject(err);
            return;
          }
          postThemeData({
            file_type: 'snippets',
            name: snippet,
            title: snippet,
            content: content
          }).then(resolve, reject);
        });
      });
      tasks.push(promise);
    });
    return Promise.all(tasks);
  };

  const uploadTemplates = function(themeConfig) {
    const tasks = [];
    (themeConfig.templates || []).forEach((template) => {
      var file = absolute_full_path(`templates/${template}.njk`);
      // vorpal.log(`read template ${template}\n    ${file}`);
      const promise = new Promise(function(resolve, reject) {
        fs.readFile(file, 'utf8', (err, content) => {
          if (err) {
            reject(err);
            return;
          }
          postThemeData({
            file_type: 'templates',
            name: template,
            title: template,
            content: content
          }).then(resolve, reject);
        });
      });
      tasks.push(promise);
    });
    return Promise.all(tasks);
  };

  const uploadPresets = function(themeConfig) {
    const uploadSinglePreset = (preset, folder) => new Promise(function(resolve, reject) {
      const themepreset = {
        name: preset,
        pageconfigs: []
      };
      listJSONFiles(folder).then(function(files) {
        const PAGE_TYPES = [
          'home', 'product', 'search', 'cart', 'blog', 'article',
          'collection_list', 'collection', 'static', 'error404'
        ];
        PAGE_TYPES.forEach((page_type) => {
          if (!_.find(files, {name: page_type})) {
            reject(new Error(`${page_type}.json is missing in preset "${preset}"`));
          }
        });
        files.forEach(function({name, data}) {
          if (name == 'theme') {
            // vorpal.log(`    ${folder}/theme.json`);
            Object.assign(themepreset, data, {
              name: themepreset.name
            });
          } else {
            // vorpal.log(`    ${folder}/${name}.json`);
            const match = name.match(/^(\w+)(\.([\w\-]+))?$/);
            const pageconfig = Object.assign({}, data, {
              page_type: match[1],
              page_name: match[3] || ''
            });
            themepreset.pageconfigs.push(pageconfig);
          }
        });
        postThemeData(themepreset, 'preset').then(resolve, reject);
      }).catch((error) => reject(error));
    });
    const tasks = [];
    (themeConfig.presets || []).forEach((preset) => {
      var folder = absolute_full_path(`presets/${preset}`);
      // vorpal.log(`read preset ${preset}`)
      const promise = uploadSinglePreset(preset, folder);
      tasks.push(promise);
    });
    return Promise.all(tasks);
  };

  const uploadLocales = function(themeConfig) {
    const tasks = [];
    (themeConfig.locales || []).forEach((locale) => {
      var file = absolute_full_path(`locales/${locale}.json`)
      if (/schema\.\w+/.test(locale)) {
        var locale = locale.substr(7);
        var scope = 'settings_schema';
      } else {
        var scope = 'theme_file';
      }
      // vorpal.log(`read locale ${locale}\n    ${file}`)
      const promise = new Promise(function(resolve, reject) {
        fs.readFile(file, 'utf8', (err, content) => {
          if (err) {
            reject(err);
            return;
          }
          var content = JSON.parse(content || '{}');
          var data = {
            scope: scope,
            language: locale,
            translations: content
          };
          postThemeData(data, 'locale').then(resolve, reject);
        });
      });
      tasks.push(promise);
    });
    return Promise.all(tasks);
  };

  const uploadBlocks = function(themeConfig) {
    var upload = (block, file, configFile) => new Promise(function(resolve, reject) {
      fs.readFile(file, 'utf8', (err, content) => {
        if (err) {
          reject(err);
          return;
        }
        fs.readFile(configFile, 'utf8', (err, configContent) => {
          if (err) {
            reject(err);
            return;
          }
          var configContent = JSON.parse(configContent || '{}')
          var settings_data = configContent.settings_data || {}
          var settings_schema = configContent.settings_schema || []
          var metafield = configContent.metafield || {}
          var presets = configContent.presets || []
          postThemeData({
            file_type: 'blocks',
            name: block,
            title: configContent.title,
            icon: configContent.icon,
            permanent: configContent.permanent,
            content: content,
            metafield: metafield,
            presets: presets,
            settings_schema: settings_schema,
            settings_data: settings_data
          }).then(resolve, reject);
        })
      })
    });
    const tasks = [];
    (themeConfig.blocks || []).forEach((block) => {
      var file = absolute_full_path(`blocks/${block}.njk`)
      var configFile = absolute_full_path(`blocks/${block}.json`)
      // vorpal.log(`read blocks ${block}\n    ${file}\n    ${configFile}`)
      const promise = upload(block, file, configFile);
      tasks.push(promise);
    });
    return Promise.all(tasks);
  }

  const uploadAssets = function(themeConfig) {
    const tasks = [];
    (themeConfig.assets || []).forEach((asset) => {
      var file = absolute_full_path(`assets/${asset}`);
      // vorpal.log(`read asset ${asset}\n    ${file}`);
      if (/\.njk$/.test(asset)) {
        var name = asset.replace(/\.njk$/, '');
        const promise = new Promise(function(resolve, reject) {
          fs.readFile(file, 'utf8', (err, content) => {
            if (err) {
              reject(err);
              return;
            }
            postThemeData({
              file_type: 'assets',
              name: name,
              title: name,
              content: content,
              src: ''
            }).then(resolve, reject);
          });
        });
        tasks.push(promise);
      } else {
        var key = `${cdnPath}/${asset}`
        var extra = new qiniu.io.PutExtra();
        const promise = new Promise(function(resolve, reject) {
          qiniu.io.putFile(qiniuUptoken, key, file, extra, function(err, ret) {
            if (err) {
              const error = new Error(JSON.stringify(err));
              reject(error);
            } else {
              vorpal.log(`\u2713 (Qiniu) asset ${key} uploaded`);
              resolve();
            }
          });
        });
        tasks.push(promise);
      }
    });
    return Promise.all(tasks);
  }

  return {
    upload: upload_theme
  }
}
