const colors = require('colors');
const _ = require('lodash');

const { validateSettingsData } = require('./block');


function printErrors(pageName, errors) {
  if (_.isEmpty(errors)) {
    console.log(pageName == 'Theme' ? 'Theme is valid' : `Page ${pageName} is valid`);
    console.log();
  } else {
    console.error(pageName == 'Theme' ? 'Theme'.red : `Page ${pageName}`.red);
    errors.forEach(function(error) {
      console.error(`    ${error}`.red);
    });
    console.error();
  }
}

function validatePageConfig(pageName, _pageconfig, blocks) {
  // console.log(`Validate block settings ${block.name}`);
  var errors = [];
  var pageconfig = _.cloneDeep(_pageconfig);

  if (!_.isObject(pageconfig.settings_data) ||
      !_.isArray(pageconfig.settings_data.components)) {
    errors.push('settings_data.components is not array');
    pageconfig.settings_data = {components: []};
  }

  if (_.has(pageconfig, 'locked') || _.has(pageconfig, 'template_name')) {
    errors.push('locked and template_name are deprecated, run "upgrade pageconfigs" first');
  } else {
    if (!pageconfig.layout) {
      errors.push('layout is required');
      if (/^(order|checkout|coupon|voucher|login|register|account)$/.test(pageName)) {
        pageconfig.layout = '__SHOPFRONT__';
        pageconfig.template = '';
      } else {
        pageconfig.layout = pageconfig.layout || 'layouts/default';
      }
    }
    if (!pageconfig.template && pageconfig.template !== '') {
      errors.push('template should be set to "" instead of undefined');
    }
  }

  pageconfig.settings_data.components = _.filter(pageconfig.settings_data.components, function(component) {
    const block = blocks[component.name];
    if (!block) {
      errors.push(`Block ${component.name} not found`);
      return false;
    } else {
      return true;
    }
  });

  pageconfig.settings_data.components.forEach(function(component) {
    const block = blocks[component.name];
    if (!_.isObject(component.settings_data)) {
      errors.push(`Block ${component.name}'s settings_data is not object`);
      component.settings_data = {};
    }
    var errs = validateSettingsData(block.settings_schema, component.settings_data, block.settings_data);
    if (!_.isEmpty(errs)) {
      errs = errs.map(err=>'    ' + err);
      errors.push(`Block ${component.name}`, ...errs);
    }
  });

  printErrors(pageName, errors);
  return pageconfig;
}

function upgradePageConfig(pageName, _pageconfig) {
  var pageconfig = _.cloneDeep(_pageconfig);
  if (_.has(pageconfig, 'template') && !_.has(pageconfig, 'locked') && !_.has(pageconfig, 'template_name')) {
    return pageconfig
  }

  if (pageconfig.locked === false) {
    pageconfig.layout = pageconfig.layout || 'layouts/default';
    pageconfig.template = pageconfig.template_name ? 'templates/' + pageconfig.template_name : '';
  } else {
    if (/^(order|checkout|coupon|voucher|login|register|account)$/.test(pageName)) {
      pageconfig.layout = '__SHOPFRONT__';
      pageconfig.template = '';
    } else {
      pageconfig.layout = pageconfig.layout || 'layouts/default';
      pageconfig.template = '__COMPONENTS__';
    }
  }
  delete pageconfig.locked;
  delete pageconfig.template_name;

  return pageconfig;
}

function validatePresetTheme(themeConfig, _theme) {
  // console.log(`Validate block settings ${block.name}`);
  var errors = [];
  var theme = _.cloneDeep(_theme);

  // Assume that themeConfig.settings_schema is validated
  const settings_schema = [];
  for (var key in themeConfig.settings_schema) {
    if (key != '__colors__') {
      var value = themeConfig.settings_schema[key];
      var field = _.assign({ id: key }, value);
      settings_schema.push(field);
    } else {
      for (var color in themeConfig.settings_schema.__colors__) {
        if (!theme.settings_data.colors[color]) {
          errors.push(`color ${color} is defined in theme's settings_schema but isn\'t set in settings_data`)
        }
      }
      for (var color in theme.settings_data.colors) {
        if (!themeConfig.settings_schema.__colors__[color]) {
          errors.push(`color ${color} is set in settings_data but isn\'t defined in theme's settings_schema`)
          delete theme.settings_data.colors[color];
        }
      }
    }
  }

  // Should check name, title, assets
  // errors.push('assets is empty');

  var errs = validateSettingsData(settings_schema, theme.settings_data, {});
  errors.push(...errs);

  printErrors('Theme', errors);
  return theme;
}

module.exports = {
  validatePresetTheme: validatePresetTheme,
  validatePageConfig: validatePageConfig,
  upgradePageConfig: upgradePageConfig
}
