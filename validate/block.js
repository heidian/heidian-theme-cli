const tinycolor = require('tinycolor2');
const colors = require('colors');
const _ = require('lodash');

const blockTypes = [
  'Text', 'Paragraph',
  'Boolean', 'Number', 'Range', 'Selection',
  'Color', 'Animation', 'Position', 'Form',
  'Image', 'Video',
  'Article', 'Collection', 'Product', 'Navigation',
  'Array', 'Mask',
  'Layout'
];

function printErrors(blockName, errors) {
  if (_.isEmpty(errors)) {
    console.log(`Block ${blockName} is valid`);
    console.log();
  } else {
    console.error(`Block ${blockName}`.red);
    errors.forEach(function(error) {
      console.error(`    ${error}`.red);
    });
    console.error();
  }
}

function getDefaultValue(field) {
  if (_.has(field, 'default')) {
    return field['default'];
  }
  switch (field.type) {
    case 'Text':
    case 'Paragraph':
      return {value: ''};
    case 'Boolean':
      return false;
    case 'Number':
    case 'Range':
      return field.min || 0;
    case 'Selection':
      // Assume that schema of field if validated
      return field.options[0].value;
    case 'Animation':
      return field.options[0].name;
    case 'Color':
      return 'rgba(0, 0, 0, 1)';
    case 'Position':
      return {top: '50%', left: '50%'};
    case 'Image':
      return {src: '', metafield: {}};
    case 'Video':
      return {src: '', cover: '', loop: false, autoplay: false, metafield: {}};
    case 'Article':
    case 'Product':
    case 'Collection':
      return {page_size: 12};
    case 'Navigation':
      return {name: 'main'};
    case 'Array':
      return [];
    case 'Form':
      return {
        button:'提交',
        list: [
          {required: false, type: 'name', title: '你的名字', icon: 'icon user'},
          {required: false, type: 'email', title: '你的邮箱', icon: 'icon mail'},
          {required: false, type: 'comment', title: '你的留言', icon: 'icon comment'}
        ]
      };
    default:
      return null;
  }
}

function validatePrimitiveField(field, settings_data) {
  var errors = [];
  const value = settings_data[field.id];
  const pushValueError = (extra) =>
    errors.push(`${field.type} field ${field.id} got invalid value` + (extra ? `, ${extra}` : ''));
  const fixValue = (value, error) => {
    pushValueError(error);
    settings_data[field.id] = value;
  };
  if (field.type == 'Text' || field.type == 'Paragraph') {
    // {value, style, href, metafield}
    var defaultValue = {
      value: _.isString(value) ? value : ''
    };
    if (!_.isObject(value)) {
      fixValue(defaultValue);
    } else if (!_.has(value, 'value')) {
      fixValue(_.defaults(value, defaultValue));
    } else if (field.is_plain_text && (_.has(value, 'style') || _.has(value, 'href'))) {
      fixValue(_.pick(value, 'value'));
    }
  } else if (field.type == 'Boolean') {
    if (!_.isBoolean(value)) {
      fixValue(!!value);
    }
  } else if (field.type == 'Number' || field.type == 'Range') {
    if (!_.isNumber(value)) {
      fixValue(+value || field.min || 0);
    }
  } else if (field.type == 'Selection' || field.type == 'Animation') {
    const key = (field.type == 'Selection' ? 'value' : 'name');
    const validValues = _.map(field.options, key);
    if (!_.includes(validValues, value)) {
      fixValue(validValues[0]);
    }
  } else if (field.type == 'Color') {
    if (!tinycolor(value).isValid()) {
      fixValue('rgba(0, 0, 0, 1)');
    }
  } else if (field.type == 'Position') {
    var defaultValue = {top: '50%', left: '50%'};
    if (!_.isObject(value)) {
      fixValue(defaultValue);
    } else if (!_.has(value, 'top') || !_.has(value, 'left')) {
      fixValue(_.defaults(value, defaultValue));
    }
  } else if (field.type == 'Form') {
    if (!_.isObject(value)) {
      // Ignore complex errors!
      fixValue(getDefaultValue(field));
    }
  } else if (field.type == 'Image') {
    // {src, metafield}
    var defaultValue = {
      src: _.isString(value) ? value : '',
      metafield: {}
    };
    if (!_.isObject(value)) {
      fixValue(defaultValue);
    } else if (!_.has(value, 'src')) {
      fixValue(_.defaults(value, defaultValue));
    }
  } else if (field.type == 'Video') {
    // {src, cover, loop, autoplay, metafield}
    var defaultValue = {
      src: _.isString(value) ? value : '',
      cover: '', loop: false, autoplay: false, metafield: {}
    };
    if (!_.isObject(value)) {
      fixValue(defaultValue);
    } else if (!_.has(value, 'src') || !_.has(value, 'loop') || !_.has(value, 'autoplay')) {
      fixValue(_.defaults(value, defaultValue));
    }
  } else if (field.type == 'Article' || field.type == 'Product' ||
             field.type == 'Collection' || field.type == 'Navigation') {
    if (!_.isObject(value)) {
      fixValue({page_size: 12});
    } else {
      if (_.has(field, 'limit') && _.has(value, 'page_size')) {
        fixValue(_.omit(value, 'page_size'), 'limit and page_size conflicts')
      }
      if (_.has(value, 'ids') || _.has(value, 'id__in') || _.has(value, 'category__in')) {
        fixValue(_.omit(value, ['ids', 'id__in', 'category__in']),
                 'ids, id__in and category__in are not allowed in preset settings')
      }
    }
  } else if (field.type == 'Navigation') {
    if (!_.isObject(value) || !_.has(value, 'name')) {
      fixValue({name: 'main'});
    }
  } else {
    // Do nothing
  }
  return errors;
}

function checkNotDefinedFields(settings_schema, settings_data) {
  var errors = [];
  var fields = [];
  (function pushFields(settings_schema) {
    settings_schema.forEach((field) => {
      if (field.type == 'Mask') {
        pushFields(field.children);
      } else if (field.type == 'Layout') {
        fields.push('__layout__');
      } else if (!field.global) {
        fields.push(field.id);
      }
    });
  })(settings_schema);
  _.keys(settings_data).forEach(function(key) {
    if (!_.includes(['style', 'colors'], key) && !_.includes(fields, key)) {
      errors.push(`${key} is set in settings_data but isn't defined in settings_schema`);
      delete settings_data[key];
    }
  });
  return errors;
}

function validateSettingsData(settings_schema, settings_data, default_settings_data={}, masked=false) {
  // assume that settings_schema is validated
  var errors = [];
  if (!masked) {
    var errs = checkNotDefinedFields(settings_schema, settings_data);
    errors.push(...errs);
  }
  settings_schema.forEach((field) => {
    // console.log(field.type, field.id)
    if (field.type == 'Mask') {
      var errs = validateSettingsData(field.children, settings_data, default_settings_data, true);
      errors.push(...errs);
    } else if (field.type == 'Layout') {
      if (!_.has(settings_data, '__layout__')) {
        errors.push('__layout__ is defined in settings_schema but isn\'t set in settings_data');
        // fix value
        _.assign(settings_data, field.presets[0]);
      }
    } else if (field.global) {
      if (_.has(settings_data, field.id)) {
        errors.push(`${field.id} is global but is set in settings_data`);
        delete settings_data[field.id];
      }
    } else if (!_.has(settings_data, field.id)) {
      errors.push(`${field.id} is defined in settings_schema but isn't set in settings_data`);
      // Assume that default_settings_data is validated
      var value = _.has(default_settings_data, field.id) ? default_settings_data[field.id] : getDefaultValue(field);
      if (value != null) {
        settings_data[field.id] = value;
      }
    } else if (field.type == 'Array') {
      settings_data[field.id].forEach(function(data) {
        // no default value for array items
        var errs = validateSettingsData(field.children, data, {});
        errors.push(...errs);
      });
    } else {
      var errs = validatePrimitiveField(field, settings_data);
      errors.push(...errs);
    }
  });
  return errors;
}

function validateSettingsSchema(settings_schema, nested=false) {
  var errors = [];
  settings_schema.forEach((field) => {
    // console.log(field.type, field.id)
    if (nested && _.includes(['Mask', 'Layout', 'Array'], field.type)) {
      // errors.push('Mask, Layout or Array could not be included in nested settings_schema');
    } else if (field.type == 'Mask') {
      var children = field.children;
      if (!_.isArray(children) || _.isEmpty(children)) {
        errors.push('A mask field got invalid or empty children');
      }
      var errs = validateSettingsSchema(children, true);
      errors.push(...errs);
    } else if (field.type == 'Layout') {
      var presets = field.presets;
      if (!_.isArray(presets) || _.isEmpty(presets)) {
        errors.push('Layout field got invalid or empty presets');
      } else {
        presets.forEach(function(preset) {
          if (!_.isObject(preset) || !_.isString(preset['__layout__'])) {
            errors.push('Layout field got invalid preset');
          }
        });
      }
    } else if (!field.id) {
      errors.push('A field misses id');
    } else if (field.id == 'style' || field.id == 'colors') {
      errors.push('field id could not be "style" nor "colors"');
    } else if (field.global) {
      // TODO: check field in theme config
    } else if (field.type == 'Array') {
      var children = field.children;
      if (!_.isArray(children) || _.isEmpty(children)) {
        errors.push(`Array field ${field.id} got invalid or empty children`);
      }
      var errs = validateSettingsSchema(children, true);
      errors.push(...errs);
    } else if (field.type == 'Selection' || field.type == 'Animation') {
      var options = field.options;
      if (!_.isArray(options) || _.isEmpty(options)) {
        errors.push(`${field.type} field ${field.id} got invalid or empty options`);
      } else {
        options.forEach(function(option) {
          if (!_.isObject(option) ||
              field.type == 'Selection' && !_.has(option, 'value') ||
              field.type == 'Animation' && !_.has(option, 'name')) {
            errors.push(`${field.type} field ${field.id} got invalid option value`);
          }
        });
      }
    } else if (field.type == 'Range') {
      if (!_.has(field, 'min') || !_.has(field, 'max') || !_.has(field, 'step')) {
        errors.push(`Properties "min", "max" and "step" must be set in the schema of Range field ${field.id}`);
      }
    } else if (!_.includes(blockTypes, field.type)) {
      errors.push(`Type of field ${field.id} is invalid`);
    } else {
      // check !
    }
  });
  return errors;
}

function validateBlockConfig(blockName, _block) {
  // console.log(`Validate block settings ${block.name}`);
  var errors = [];
  var block = _.cloneDeep(_block);

  if (!/^blocks\/[\w\-]+$/.test(block.name) || block.name.substr(7) != blockName) {
    errors.push(`name is not valid, got "${block.name}"`);
    block.name = `blocks/${blockName}`;
  }

  if (!_.isArray(block.settings_schema)) {
    errors.push('settings_schema is not array');
    block.settings_schema = [];
  }

  if (!_.isObject(block.settings_data)) {
    errors.push('settings_data is not object');
    block.settings_data = {};
  }

  var errs = validateSettingsSchema(block.settings_schema);
  errors.push(...errs);

  if (_.isEmpty(errs)) {
    errs = validateSettingsData(block.settings_schema, block.settings_data, {});
    errors.push(...errs);
  } else {
    errors.push('settings_schema got errors, fix them first');
  }

  printErrors(blockName, errors);
  return block;
}

module.exports = {
  getDefaultValue: getDefaultValue,
  validatePrimitiveField: validatePrimitiveField,
  validateSettingsData: validateSettingsData,
  validateSettingsSchema: validateSettingsSchema,
  validateBlockConfig: validateBlockConfig
}
