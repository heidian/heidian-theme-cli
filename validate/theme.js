const colors = require('colors');
const _ = require('lodash');

const { validateSettingsSchema, validateSettingsData } = require('./block');


function printErrors(errors) {
  if (_.isEmpty(errors)) {
    console.log('Theme config is valid');
  } else {
    if (_.isEmpty(errors)) return;
    console.error(`Theme`.red);
    errors.forEach(function(error) {
      console.error(`    ${error}`.red);
    });
    console.error();
  }
}

function validateThemeConfig(_themeConfig) {
  // console.log(`Validate block settings ${block.name}`);
  var errors = [];
  var themeConfig = _.cloneDeep(_themeConfig);

  if (!_.isObject(themeConfig.settings_schema)) {
    errors.push('settings_schema is not object');
    themeConfig.settings_schema = {};
  }

  // Should check layouts, blocks, assets, locales, language, snippets, presets, current_preset

  const settings_schema = [];
  for (var key in themeConfig.settings_schema) {
    if (key != '__colors__') {
      var value = themeConfig.settings_schema[key];
      var field = _.assign({ id: key }, value);
      settings_schema.push(field);
    }
  }

  var errs = validateSettingsSchema(settings_schema);
  errors.push(...errs);

  settings_schema.map((field) => {
    themeConfig.settings_schema[field.id] = _.omit(field, 'id');
  });

  printErrors(errors);
  return themeConfig;
}

module.exports = {
  validateThemeConfig: validateThemeConfig
}
