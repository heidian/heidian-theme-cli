const fs = require('fs');
const path = require('path');
const colors = require('colors');
const _ = require('lodash');

const {
  absolute_full_path,
  writeJSONSync,
  getJSONFile,
  listJSONFiles
} = require('../utils');
const { validateBlockConfig } = require('./block');
const { validatePresetTheme, validatePageConfig, upgradePageConfig } = require('./preset');
const { validateThemeConfig } = require('./theme');


module.exports = function(vorpal) {

  function validateTheme(autoFix) {
    // Validate settings_schema of theme
    const dryRun = !autoFix;
    const fullPathName = absolute_full_path('theme.json');
    return new Promise(function(resolve, reject) {
      getJSONFile(fullPathName).then(function(data) {
        const validatedData = validateThemeConfig(data);
        if (!dryRun) {
          writeJSONSync(fullPathName, validatedData)
        }
        resolve();
      }).catch((err) => reject(err));
    });
  }

  function validateBlocks(autoFix) {
    // Validate settings_schema and settings_data of blocks
    const dryRun = !autoFix;
    const folderPath = absolute_full_path('blocks');
    return new Promise(function(resolve, reject) {
      listJSONFiles(folderPath).then(function(files) {
        files.forEach(function({name, data}) {
          const validatedData = validateBlockConfig(name, data);
          if (!dryRun) {
            writeJSONSync(`${folderPath}/${name}.json`, validatedData)
          }
          resolve();
        });
      }).catch((err) => reject(err));
    });
  }

  function validatePreset(presetName, autoFix) {
    // Validate settings_data of pageconfigs and theme
    const dryRun = !autoFix;
    const themeConfigPath = absolute_full_path('theme.json');
    const blocksFolderPath = absolute_full_path('blocks');
    const presetFolderPath = absolute_full_path(`presets/${presetName}`);
    return new Promise(function(resolve, reject) {
      Promise.all([
        getJSONFile(themeConfigPath),
        listJSONFiles(blocksFolderPath),
        listJSONFiles(presetFolderPath)
      ]).then(function([themeConfig, blockFiles, presetFiles]) {
        const PAGE_TYPES = [
          'home', 'product', 'search', 'cart', 'blog', 'article',
          'collection_list', 'collection', 'static', 'error404'
        ];
        PAGE_TYPES.forEach((page_type) => {
          if (!_.find(presetFiles, {name: page_type})) {
            vorpal.log(`Page ${page_type}\n    ${page_type}.json is missing\n`.red);
          }
        });
        const blocks = _.fromPairs(blockFiles.map(file => [file.data.name, file.data]));
        presetFiles.forEach(function({name, data}) {
          var validatedData;
          if (name == 'theme') {
            validatedData = validatePresetTheme(themeConfig, data);
          } else {
            validatedData = validatePageConfig(name, data, blocks);
          }
          if (!dryRun) {
            writeJSONSync(`${presetFolderPath}/${name}.json`, validatedData)
          }
        });
        resolve();
      }).catch((err) => reject(err));
    });
  }

  function upgradePageConfigs() {
    // Upgrade pageconfigs to use new format (drop "locked" and "template_name").
    const promises = [];
    const presetsFolderPath = absolute_full_path('presets');
    const folders = fs.readdirSync(presetsFolderPath);
    folders.forEach((presetName) => {
      const presetFolderPath = absolute_full_path(`presets/${presetName}`);
      if (fs.lstatSync(presetFolderPath).isDirectory()) {
        // console.log(presetName)
        const promise = new Promise(function(resolve, reject) {
          Promise.all([
            listJSONFiles(presetFolderPath)
          ]).then(function([presetFiles]) {
            presetFiles.forEach(function({name, data}) {
              var validatedData = upgradePageConfig(name, data);
              writeJSONSync(`${presetFolderPath}/${name}.json`, validatedData);
            });
            resolve();
          }).catch((err) => reject(err));
        });
        promises.push(promise)
      }
    })
    return Promise.all(promises)
  }

  return {
    validateTheme: validateTheme,
    validateBlocks: validateBlocks,
    validatePreset: validatePreset,
    upgradePageConfigs: upgradePageConfigs
  }
}
