# Heidian Theme Uploader

> 嘿店模板上传工具

## Install

```
npm install -g heidian-theme-cli
```

## Usage

```bash
heidian-theme
```

## License

MIT
