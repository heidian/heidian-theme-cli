const fs = require('fs');
const path = require('path');
const colors = require('colors');

const { absolute_full_path, writeJSONSync } = require('../utils');


module.exports = function(vorpal) {

  function formatJSONFile(fileName) {
    vorpal.log(`Format ${fileName}`)
    return new Promise(function(resolve, reject) {
      fs.readFile(fileName, 'utf8', (err, content) => {
        if (err) {
          reject(err);
          return;
        }
        try {
          const data = JSON.parse(content);
          writeJSONSync(fileName, data);
        } catch (e) {
          vorpal.log(`Parse error: ${fileName}`.red);
          reject(e);
        }
        resolve();
      });
    });
  }

  function listAndFormatJSONFiles(folderPath) {
    return new Promise(function(resolve, reject) {
      fs.readdir(folderPath, (err, items) => {
        if (err) {
          reject(err);
          return;
        }
        const promises = [];
        items.forEach((item) => {
          if (/\.json$/.test(item)) {
            promises.push(formatJSONFile(`${folderPath}/${item}`));
          }
        });
        Promise.all(promises).then(resolve, reject);
      });
    });
  }

  function run() {
    return new Promise(function(resolve, reject) {
      const promises = [];
      promises.push(formatJSONFile(absolute_full_path('theme.json')));
      promises.push(listAndFormatJSONFiles(absolute_full_path('blocks')));
      const folderPath = absolute_full_path('presets');
      fs.readdir(folderPath, (err, items) => {
        if (err) {
          reject(err);
          return;
        }
        items.forEach((item) => {
          const presetFolderPath = `${folderPath}/${item}`;
          const promise = new Promise(function(resolve, reject) {
            fs.lstat(presetFolderPath, (err, stats) => {
              if (err) {
                reject(err);
                return;
              }
              if (stats.isDirectory()) {
                listAndFormatJSONFiles(presetFolderPath).then(resolve, reject);
              }
            });
          });
          promises.push(promise);
        });
        Promise.all(promises).then(resolve, reject);
      });
    });
  }

  return {
    run: run
  }
}
