const fs = require('fs');
const path = require('path');


function absolute_full_path(filePath, themeFolder, rootPath) {
  themeFolder || (themeFolder = 'theme');
  rootPath || (rootPath = process.cwd());
  return path.join(rootPath, themeFolder, filePath)
}

function writeJSONSync(fullPathName, data) {
  const content = JSON.stringify(data, null, 2);
  fs.writeFileSync(fullPathName, content + '\n', 'utf8');
}

function readJSONSync(fullPathName) {
  const content = fs.readFileSync(fullPathName, 'utf8');
  return JSON.parse(content);
}

function getJSONFile(fullPath) {
  return new Promise(function(resolve, reject) {
    fs.readFile(fullPath, 'utf8', (err, content) => {
      if (err) {
        reject(err);
      } else {
        var data = JSON.parse(content);
        resolve(data);
      }
    });
  });
}

function listJSONFiles(folderPath) {
  return new Promise(function(resolve, reject) {
    const readFiles = function(items) {
      const promises = [];
      items.forEach((item) => {
        if (/\.json$/.test(item)) {
          const fullPathName = `${folderPath}/${item}`;
          const promise = new Promise(function(resolve, reject) {
            fs.readFile(fullPathName, 'utf8', (err, content) => {
              if (err) {
                reject(err);
              } else {
                var data = JSON.parse(content);
                var name = item.slice(0, -5);
                resolve({name: name, data: data});
              }
            });
          });
          promises.push(promise);
        }
      });
      Promise
        .all(promises)
        .then((values) => resolve(values))
        .catch((err) => reject(err))
    };
    fs.readdir(folderPath, (err, items) => {
      if (err) {
        reject(err);
      } else {
        readFiles(items);
      }
    });
  });
}

module.exports = {
  absolute_full_path: absolute_full_path,
  writeJSONSync: writeJSONSync,
  readJSONSync: readJSONSync,
  getJSONFile: getJSONFile,
  listJSONFiles: listJSONFiles
}
