const os = require('os')
const path = require('path');
const axios = require('axios');
const _ = require('lodash');

const { readJSONSync, writeJSONSync } = require('../utils');


const SECRETS = new (function() {
  this.data = {}
  const secretFilePath = path.join(os.homedir(), '.heidian-theme');
  try {
    const content = readJSONSync(secretFilePath);
    this.data = content
  } catch (err) {
    if (err.code === 'ENOENT') {
      writeJSONSync(secretFilePath, {});
    } else {
      throw err;
    }
  }
  this.get = function(shopName) {
    const secret = _.cloneDeep(this.data[shopName] || {});
    if (secret.accessToken && secret.expiresAt && secret.expiresAt > new Date().valueOf()) {
      secret.expiresIn = secret.expiresAt - new Date().valueOf()
      return secret;
    }
  }
  this.set = function(shopName, accessToken) {
    const expiresAt = new Date().valueOf() + 7 * 86400 * 1000; // 先写死一周
    this.data[shopName] = {
      accessToken: accessToken,
      expiresAt: expiresAt
    }
    writeJSONSync(secretFilePath, this.data);
  }
})();


module.exports = function(vorpal, apiOrigin) {
  function parseAxiosError(error) {
    if (error.response) {
      const {config, status, data} = error.response;
      return new Error(`axios error ${status} ${config.url}\n${JSON.stringify(data)}`);
    } else {
      return error;
    }
  }

  const api = axios.create({
    baseURL: `${apiOrigin}/api/`
  });

  function login(shopName, email, password) {
    return new Promise(function(resolve, reject) {
      const data = `username=${email}&password=${password}`;
      const promise = api.post('/oauth/dashboard/token/', data, {
        headers: {
          'x-shop': shopName,
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
      promise.then(function (res) {
        const accessToken = res.data.access_token;
        SECRETS.set(shopName, accessToken);
        vorpal.log(`\u2713 Login success to shop ${shopName}\n`);
        resolve();
      }).catch((error) => {
        reject(parseAxiosError(error));
      });
    });
  }

  function getSecret(shopName) {
    return SECRETS.get(shopName);
  }

  return {
    login: login,
    getSecret: getSecret
  }
}
