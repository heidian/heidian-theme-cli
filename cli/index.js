const _ = require('lodash');
const Vorpal = require('vorpal');

const { absolute_full_path, readJSONSync } = require('../utils')
const Formatter = require('../format');
const Validator = require('../validate');
const Downloader = require('../download');
const Uploader = require('../upload');
const Authenticator = require('../auth');


module.exports = function(options) {
  options || (options = {});
  const apiOrigin = options.apiOrigin || 'https://heidianapi.com';

  const vorpal = Vorpal();
  const chalk = vorpal.chalk;
  const themeConfig = readJSONSync(absolute_full_path('theme.json'));

  const formatter = Formatter(vorpal);
  const validator = Validator(vorpal);
  const downloader = Downloader(vorpal, apiOrigin);
  const uploader = Uploader(vorpal, apiOrigin);
  const authenticator = Authenticator(vorpal, apiOrigin);

  vorpal
    .command('format', 'Format JSON files.')
    .action(function(args, callback) {
      formatter.run()
        .then(() => callback())
        .catch((error) => {
          this.log(chalk.red(error.stack));
          callback();
        });
    });

  vorpal
    .command('validate <type> [name]', 'Validate configs of theme, blocks and preset.')
    .option('-f, --fix', 'Try to fix invalid data')
    .autocomplete(['theme', 'blocks', 'preset'])
    .validate(function (args) {
      if (!_.includes(['theme', 'blocks', 'preset'], args.type)) {
        return 'type should be "theme", "blocks" or "preset"';
      } else if (args.type == 'preset' && !args.name) {
        return 'preset name is required';
      } else {
        return true;
      }
    })
    .action(function(args, callback) {
      const autoFix = !!args.options.fix;
      if (args.type == 'theme') {
        validator.validateTheme(autoFix)
          .then(() => callback())
          .catch((error) => {
            // this.log(chalk.red(error));
            this.log(chalk.red(error.stack));
            callback();
          });
      } else if (args.type == 'blocks') {
        validator.validateBlocks(autoFix)
          .then(() => callback())
          .catch((error) => {
            // this.log(chalk.red(error));
            this.log(chalk.red(error.stack));
            callback();
          });
      } else if (args.type == 'preset') {
        validator.validatePreset(args.name, autoFix)
          .then(() => callback())
          .catch((error) => {
            // this.log(chalk.red(error));
            this.log(chalk.red(error.stack));
            callback();
          });
      }
    });

  vorpal
    .command('upgrade pageconfigs', 'Upgrade pageconfigs to use new format (drop "locked" and "template_name").')
    .action(function(args, callback) {
      validator.upgradePageConfigs()
        .then(() => callback())
        .catch((error) => {
          // this.log(chalk.red(error));
          this.log(chalk.red(error.stack));
          callback();
        });
    });

  vorpal
    .command('download preset <presetName>', 'Download a shops\'s theme and pages config to preset')
    .action(function(args, callback) {
      this.prompt({
        type: 'input',
        name: 'shopName',
        message: 'Name of the shop you want to download from: '
      }).then((result) => {
        const { presetName } = args;
        const { shopName } = result;
        downloader.listThemes(shopName).then((themes) => {
          this.prompt({
            type: 'list',
            name: 'themeId',
            choices: themes.map((theme) => ({
              name: `[${theme.scope}][${theme.id}] ${theme.title} | ${theme.name}` + (theme.private ? ' (private)' : ''),
              value: theme.id
            })),
            message: 'Choose theme to download'
          }).then((result) => {
            const { themeId } = result;
            this.log(`Download configs from theme ${themeId} of ${shopName} to preset ${presetName}`);
            downloader.downloadPreset(shopName, themeId, presetName)
              .then(() => callback())
              .catch((error) => {
                // this.log(chalk.red(error));
                this.log(chalk.red(error.stack));
                callback();
              });
          });
        });
      });
    });

  vorpal
    .command('download themefiles', 'Download a shops\'s layouts, blocks, templates and snippets (except assets)')
    .action(function(args, callback) {
      this.prompt({
        type: 'input',
        name: 'shopName',
        message: 'Name of the shop you want to download from: '
      }).then((result) => {
        const { shopName } = result;
        downloader.listThemes(shopName).then((themes) => {
          this.prompt({
            type: 'list',
            name: 'themeId',
            choices: themes.map((theme) => ({
              name: `[${theme.scope}][${theme.id}] ${theme.title} | ${theme.name}` + (theme.private ? ' (private)' : ''),
              value: theme.id
            })),
            message: 'Choose theme to download'
          }).then((result) => {
            const { themeId } = result;
            this.log(`Download themefiles from theme ${themeId} of ${shopName}`);
            downloader.downloadThemeFiles(shopName, themeId)
              .then(() => callback())
              .catch((error) => {
                // this.log(chalk.red(error));
                this.log(chalk.red(error.stack));
                callback();
              });
          });
        });
      });
    });

  // vorpal
  //   .command('upload theme', 'Upload theme to shop')
  //   .action(function(args, callback) {
  //     this.prompt({
  //       type: 'input',
  //       name: 'shopName',
  //       message: 'Name of the shop you want to upload to: '
  //     }).then((result) => {
  //       const { shopName } = result;
  //       downloader.listThemes(shopName).then((themes) => {
  //         this.prompt([{
  //           type: 'list',
  //           name: 'themeId',
  //           choices: themes.map((theme) => ({
  //             name: `[${theme.scope}][${theme.id}] ${theme.title} | ${theme.name}` + (theme.private ? ' (private)' : ''),
  //             value: theme.id
  //           })),
  //           message: 'Choose theme to override'
  //         }, {
  //           type: 'input',
  //           name: 'accessToken',
  //           message: 'Access token of the shop: '
  //         }]).then((result) => {
  //           const { themeId, accessToken } = result;
  //           this.log(`Upload to theme ${themeId} of ${shopName}`);
  //           uploader.uploadTheme(shopName, themeId, accessToken)
  //             .then(() => callback())
  //             .catch((error) => {
  //               // this.log(chalk.red(error));
  //               this.log(chalk.red(error.stack));
  //               callback();
  //             });
  //         });
  //       });
  //     });
  //   });

  vorpal
    .command('upload theme', 'Upload theme to shop')
    .action(function(args, callback) {
      this.prompt({
        type: 'input',
        name: 'shopName',
        message: 'Name of the shop you want to upload to: '
      }).then((result) => {
        const { shopName } = result;
        const secret = authenticator.getSecret(shopName);
        if (!secret) {
          const msg = `Shop ${shopName} is not authenticated. \n  (use "login ${shopName}" to authenticate before uploading the theme) \n`;
          this.log(chalk.red(msg));
          callback();
          return;
        }
        const { accessToken, expiresIn } = secret;
        const msg = `Shop ${shopName} is authenticated. Expires in ${parseInt(expiresIn/1000)} seconds.\n`;
        this.log(chalk.cyan(msg));
        downloader.listThemes(shopName).then((themes) => {
          this.prompt({
            type: 'list',
            name: 'themeId',
            choices: themes.map((theme) => ({
              name: `[${theme.scope}][${theme.id}] ${theme.title} | ${theme.name}` + (theme.private ? ' (private)' : ''),
              value: theme.id
            })),
            message: 'Choose theme to override'
          }).then((result) => {
            const { themeId } = result;
            this.log(`Upload to theme ${themeId} of ${shopName}`);
            uploader.uploadTheme(shopName, themeId, accessToken)
              .then(() => callback())
              .catch((error) => {
                // this.log(chalk.red(error));
                this.log(chalk.red(error.stack));
                callback();
              });
          });
        });
      });
    });

  vorpal
    .command('publish theme', 'Publish theme to Heidian themes store')
    .action(function(args, callback) {
      this.log(`Publish theme`);
      uploader.publishTheme()
        .then(() => callback())
        .catch((error) => {
          // this.log(chalk.red(error));
          this.log(chalk.red(error.stack));
          callback();
        });
    });

  vorpal
    .command('login <shopName>', 'Authorize for shop')
    .action(function(args, callback) {
      const { shopName } = args;
      this.prompt([{
        type: 'input',
        name: 'email',
        message: `Email of shop ${shopName}'s admin user: `
      }, {
        type: 'password',
        name: 'password',
        message: `Password of shop ${shopName}'s admin user: `
      }]).then((result) => {
        const { email, password } = result;
        this.log(`Authentication for ${email} of ${shopName}`);
        authenticator.login(shopName, email, password)
          .then(() => callback())
          .catch((error) => {
            // this.log(chalk.red(error));
            this.log(chalk.red(error.stack));
            callback();
          });
      });
    });

  vorpal
    // .delimiter('heidian$')
    .delimiter(`${chalk.cyan.bold('heidian-theme')} ${chalk.magenta.bold(themeConfig.name)} $`)
    .show();
}
